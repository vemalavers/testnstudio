const bghttp = require('@nativescript/background-http');
const session = bghttp.session('image-upload');

const ls = require('~/common/ls');
const files = require('~/common/files');

function bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
}

function onEvent(e) {
    console.log({
        color: {
            eventName: e.eventName,
            responseCode: e.responseCode
        },
        eventTitle: e.eventName + " " + e.object.description,
        eventData: JSON.stringify({
            error: e.error ? e.error.toString() : e.error,
            currentBytes: e.currentBytes,
            totalBytes: e.totalBytes,
            body: e.data,
            responseCode: e.responseCode
        })
    });
    switch (e.eventName) {
        case 'complete':
            console.log('========= BG complete ========= ', e.responseCode);
            setNextLoop(e.object.description, e.responseCode);
            break;
        case 'error':
            console.log('========= BG error ============', e.responseCode);
            setNextLoop(e.object.description, e.responseCode);
            break;
        default:
            break;
    }
}


function setNextLoop(file_name, status) {
    ls.setNumber('bg_loop_status', status);
    deleteLsJsonContent(file_name);
}
exports.nextLoop = function(file_name) {
    setNextLoop(file_name);
}

function getNameFromFile(item) {
    const last = item.lastIndexOf('/');
    return item.substring(last + 1, item.length);
}

function setLsJsonContent(file_name, content, path) {
    ls.setJson(file_name, {
        path: path,
        content: content
    });
}

function deleteLsJsonContent(file_name) {
    ls.remove(file_name);
}

function detectFinishBackground() {
    const promise = new Promise((resolve, reject) => {
        const interval = setInterval(() => {
            const status = ls.getNumber('bg_loop_status');
            console.log('status interval:', status);
            switch (status) {
                case 0:
                    //next loop
                    //reject(status);
                    break;
                case 200:
                    clearInterval(interval);
                    resolve(status);
                    break;
                case 201:
                    clearInterval(interval);
                    resolve(status);
                    break;
                default:
                    clearInterval(interval);
                    reject(status);
                    break;
            }
        }, 1000);
    });
    return promise;
}

function getParamsMultipart(items) {
    const content = [];
    for (const k in items) {
        content.push({
            name: k,
            value: items[k]
        })
    }
    return content;
}

exports.sendBackgroundFiles = function(url, method, content, path, isMultipart, name_in_url) {
    if (isMultipart) {
        return multipartUpload(url, method, content, path, name_in_url);
    } else {
        return singleUpload(url, method, content, path, name_in_url);
    }
}

function singleUpload(url, method, content, path, name_in_url) {
    console.log('singleUpload(()');
    const promise = new Promise((resolve, reject) => {

        const size = files.getSizeFile(path);
        const file_name = getNameFromFile(path);

        if (size > 0) {
            setLsJsonContent(file_name, content, path);
            console.log('size: ' + file_name + ' = ' + bytesToSize(size));
            console.log(method, url);
            console.log(path);
            console.log(content);
            console.log('name_in_url', name_in_url);
            const request = {
                url: url,
                method: method,
                headers: {
                    'x-device-type': 1,
                    'Authorization': 'Bearer ' + ls.getString('token'),
                    'Content-Type': 'application/octet-stream', //form-data
                    'file-name': name_in_url ? name_in_url : file_name,
                    'Content': JSON.stringify(content)
                },
                description: name_in_url ? name_in_url : file_name,
                AndroidAutoClearNotificación: true
            };
            const task = session.uploadFile(path, request);

            ls.setNumber('bg_loop_status', 0);
            task.on('progress', onEvent);
            task.on('error', onEvent);
            task.on('responded', onEvent);
            task.on('complete', onEvent);
            task.on('cancelled', onEvent); // Android only
        } else {
            console.log('====== DELETE SIZE 0 ========', path);
            reject('sise 0');
        }

        detectFinishBackground()
            .then((r) => {
                console.log('statusA:', r);
                resolve(r);
            })
            .catch((e) => {
                console.log('errorA:', e);
                reject(e);
            });
    });
    return promise;
}

function multipartUpload(url, method, content, path, name_in_url) {
    console.log('multipartUpload()');
    const promise = new Promise((resolve, reject) => {

        const size = files.getSizeFile(path);
        const file_name = getNameFromFile(path);

        if (size > 0) {
            setLsJsonContent(file_name, content, path);
            console.log('size: ' + file_name + ' = ' + bytesToSize(size));
            console.log(method, url);
            console.log(path);
            console.log(content);
            console.log('name_in_url', name_in_url);
            const request = {
                url: url,
                method: method,
                headers: {
                    'x-device-type': 1,
                    'Authorization': 'Bearer ' + ls.getString('token'),
                    'Content-Type': 'multipart/form-data', // 'multipart/form-data' 'form-data' 'application/octet-stream'
                    'file-name': name_in_url ? name_in_url : file_name
                },
                utf8: true,
                description: name_in_url ? name_in_url : file_name,
                AndroidAutoClearNotificación: true
            };
            const params = getParamsMultipart(content);
            params.push({ name: 'file', filename: path, mimeType: 'image/jpg' });

            const task = session.multipartUpload(params, request);

            ls.setNumber('bg_loop_status', 0);
            task.on('progress', onEvent);
            task.on('error', onEvent);
            task.on('responded', onEvent);
            task.on('complete', onEvent);
            task.on('cancelled', onEvent); // Android only 
        } else {
            console.log('====== DELETE SIZE 0 ========', path);
            reject('sise 0');
        }

        detectFinishBackground()
            .then((r) => {
                console.log('statusA:', r);
                resolve(r);
            })
            .catch((e) => {
                console.log('errorA:', e);
                reject(e);
            });
    });
    return promise;
}

/*
Add line in 270 background-http.android.js
function setRequestOptions(request, options) {
    //FIX TIMEOUT => 5min
    net.gotev.uploadservice.UploadService.HTTP_STACK = new  net.gotev.uploadservice.http.impl.HurlStack(true, false, 5*60*1000, 5*60*1000);
    //
*/