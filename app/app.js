import { Application } from '@nativescript/core';

Application.run({ moduleName: 'app-root' })

const { init } = require('@nativescript/background-http');
init();