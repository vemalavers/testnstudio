const Http = require('@nativescript/core/http');
const ViewModel = require('./background-api-view-model');

const ls = require('~/common/ls');
const session_config = require('~/common/session');
const { VideoEditor } = require('@viia/nativescript-video-editor');

const SocialShare = require('@nativescript/social-share');

const Dialogs = require('@nativescript/core');

let page;
exports.onNavigatingTo = function navigatingTo(args) {
    page = args.object;
    setInit();
    page.bindingContext = ViewModel;
}

function setInit() {
    console.log('========== setInit() ===========')
    ViewModel.set('file_url', undefined);
    ViewModel.set('listView', []);
    ViewModel.set('endpoint', 'pop/proofs');
    ViewModel.set('loaded', true);
    ViewModel.set('loaded_login', true);
    ViewModel.set('user', '');
    ViewModel.set('pass', '');

    getTokenLS();
    setUrl(7);
    setContentType(0);
    setContent();
}

function getTokenLS() {
    const token = ls.getString('token');
    if (token) {
        ViewModel.set('token', token);
        ViewModel.set('user', ls.getString('user'));
        ViewModel.set('pass', ls.getString('pass'));
    }
}

function setUrl(value) {
    let urls;
    urls = ['https://upload.viiamanager.com/', 'https://dev-upload.viiamanager.com/', 'http://127.0.0.1/', 'https://lambda.viiamanager.com/upload/mobile/', 'https://dev-lambda.viiamanager.com/upload/mobile/', 'http://127.0.0.1/upload/mobile/', 'https://uploads.viiamanager.com/', 'https://dev-uploads.viiamanager.com/', 'http://127.0.0.1/upload.php/', 'https://upload.viiamanager.com/upload.php/', 'https://dev-upload.viiamanager.com/upload.php/', 'http://127.0.0.1/upload.php/'];
    ViewModel.set('url', urls[value]);
}

function setContent(captured_at) {
    let content = {};
    switch (ViewModel.get('endpoint')) {
        case 'vta':
            content = {
                type: 'task',
                name: 1,
                order_id: 1,
                task_id: 1,
                activity_id: 1
            }
            break;
        case 'videochecking/custom/videos': //dev
            content = {
                devices_id: 347,
                media_id: '1',
                captured_at: captured_at ? captured_at : '2022-09-13 14:15:16',
                devices_faces_id: 368
            }
            break;
        default: //pop dev
            content = {
                //image_url: 'viia-pop-dev/1/proofs/2022/09/13/1/1/20220913_1_1_1_1_1_1_1-1.jpg',
                licences_id: 1,
                units_id: 9142,
                units_faces_id: 24554,
                instructions_id: 5,
                captured_at: captured_at ? captured_at : '2022-09-13 14:15:16',
                //arts_id: 1, //optional
                started_at: captured_at ? captured_at : '2022-09-13 14:15:16', // optional
                // coordinates: ['37.784104249651', '-122.40819798679'] // optional
            }
            break;
    }

    ViewModel.set('content', JSON.stringify(content));
}

function getContent(filename) {
    let item = ViewModel.get('content');
    if (item) {
        item = JSON.parse(item);
        item.type = ViewModel.get('endpoint').trim();
        if (filename) {
            item.filename = filename;
        }
        ViewModel.set('content', JSON.stringify(item));
    }
    return item;
}
exports.tapSetUrl = function(args) {
    const item = args.object;
    setUrl(item.url);
}
exports.tapSetEndPoint = function(args) {
    const item = args.object;
    ViewModel.set('endpoint', item.text);
    getContent();
}

function setContentType(value) {
    const contentType = ['multipart/form-data', 'application/octet-stream', 'form-data'];
    ViewModel.set('contentType', contentType[value]);
}
exports.tapSetPlatform = function(args) {
    const item = args.object;
    ViewModel.set('platform', item.platform);
}
exports.tapSetContentType = function(args) {
    const item = args.object;
    setContentType(item.contentType);
}

function getDurationInSeconds(ms) {
    return parseInt(ms / 1000);
}

function getCapturedAt(name) {
    name = name.replace('VID_', '').replace('.mp4', '');
    name = name.replace('PIC_', '').replace('.jpg', '');

    const y = name.substring(0, 4);
    const m = name.substring(4, 6);
    const d = name.substring(6, 8);
    const hour = name.substring(8, 10);
    const min = name.substring(10, 12);
    const seg = name.substring(12, 14);
    return y + '-' + m + '-' + d + ' ' + hour + ':' + min + ':' + seg;
}

exports.tapGetFile = function(args) {
    const btn = args.object;
    btn.isEnabled = false;
    const mode = btn.type;
    //const fileName = '1_1454_119_20210513_130658'; //Date.now();
    console.log(mode);
    const config = {
        saveToGallery: false,
        maxPhotoCount: 1,
        maxVideoCount: 1,
        mode: mode, //'PHOTO' | 'VIDEO' | 'BOTH';
        pictureSize: '800x600', // will try to use the closest supported size
        ratio: '4:3', //'4:3,16:9' the underlaying camerax supports these only
        filePath: ls.getString('path_tmp'),
        //fileName: fileName,
        enableAudio: false,
        videoQuality: 'HIGHEST',
        enableAttach: true, // hide the attach button
        showVideoEditor: false, // hide video timing functionality
    };
    console.log(config);
    VideoEditor.openCamera(config)
        .then((r) => {
            console.log(r);

            btn.isEnabled = true;
            r = r[0];

            ViewModel.set('listView', []);
            ViewModel.set('file_url', r.file);
            ViewModel.set('file_name', r.name);
            ViewModel.set('size', r.sizeInBytes);
            const duration = r.type == 'Video' ? getDurationInSeconds(r.duration) : 0;
            ViewModel.set('duration', duration);

            if (r.type == 'Video') {
                ViewModel.set('file_type', 'video');
            } else {
                ViewModel.set('file_type', 'image');
            }

            ls.setJson('last', {
                file_src: r.file,
                file_name: r.name,
                file_size: r.sizeInBytes,
                file_duration: duration
            });

            const captured_at = getCapturedAt(r.name);
            setContent(captured_at);

        })
        .catch((e) => {
            btn.isEnabled = true;
            console.log(e);
        });
}

function pushMessage(color, eventTitle, eventData) {
    const listView = ViewModel.get('listView');
    listView.unshift({
        color: color,
        eventTitle: eventTitle,
        eventData: eventData
    });
    ViewModel.set('listView', listView);
    page.getViewById('listView').refresh();
}

exports.tapPreview = function() {
    setContent();
}

exports.login = function() {
    Dialogs.login({
        title: "LOGIN",
        message: "User and password",
        okButtonText: "Login in Prod",
        cancelButtonText: "Login in Dev",
        neutralButtonText: "Cancel",
        userName: ViewModel.get('user'),
        password: ViewModel.get('pass')
    }).then(function(r) {
        switch (true) {
            case r.result === true:
                sendLogin(r.userName, r.password, 'prod');
                break;
            case r.result === false:
                sendLogin(r.userName, r.password, 'dev');
                break;
            default: //undefined
                break;
        }
    });
}

function sendLogin(user, pass, environment) {
    const data = {
        'email': user.toLowerCase(),
        'password': pass
    }
    if (environment == 'prod') {
        session_config.setConfig('android', 31, false);
    } else {
        session_config.setConfig('android', 31, true);
    }
    ViewModel.set('environment', environment);
    ViewModel.set('token', undefined);
    ViewModel.set('loaded_login', false);
    const url = 'session';
    Http.request(ls.request(url, 'POST', data)).then(function(r) {
        ViewModel.set('loaded_login', true);
        const item = JSON.parse(r.content);
        if (r.statusCode == 201) {
            ls.setString('user', user);
            ls.setString('pass', pass);
            ls.setString('token', item.data.token);

            ViewModel.set('user', user);
            ViewModel.set('pass', pass);
            ViewModel.set('token', item.data.token);
        } else {
            // eslint-disable-next-line no-undef
            alert('Error de sesión ' + item.message);
        }
    }, function(e) {
        console.log(e);
        ViewModel.set('loaded_login', true);
        // eslint-disable-next-line no-undef
        alert('Error de internet, verifique su conexión');
    });
}

function validate() {
    let isok = true;
    const file_url = ViewModel.get('file_url');
    const file_name = ViewModel.get('file_name');
    const size = ViewModel.get('size');

    const url = ViewModel.get('url');
    const endpoint = ViewModel.get('endpoint');
    if (!url) {
        isok = false;
        pushMessage('red', 'validación', 'Sin dominio');
    }
    if (!endpoint) {
        isok = false;
        pushMessage('red', 'validación', 'Sin endpoint');
    }
    if (!file_url) {
        isok = false;
        pushMessage('red', 'validación', 'Sin archivo');
    }
    if (!file_name) {
        pushMessage('red', 'validación', 'Sin nombre de archivo');
        isok = false;
    }
    if (!size) {
        pushMessage('red', 'validación', 'Sin tamaño del archivo');
        isok = false;
    }

    return isok;
}
exports.tapSend = function() {
    ViewModel.set('listView', []);
    if (!validate()) {
        return;
    }
    //SERVICE
    const url = ViewModel.get('url') + ViewModel.get('endpoint');
    const method = 'POST';
    let content = ViewModel.get('content');
    content = content ? JSON.parse(content) : content;
    //FILE
    ViewModel.set('loaded', false);
    const file_url = ViewModel.get('file_url');
    const file_name = ViewModel.get('file_name');
    //const size = ViewModel.get('size');
    //const duration = ViewModel.get('duration');

    const multipart = page.getViewById('switch_type_send').checked ? true : false;

    const json_file = {
        url: url,
        method: method,
        name: file_name,
        content: content,
        path: file_url,
        multipart: multipart,
        name_in_url: false
    }
    console.log(json_file);

    sendFiles(json_file);
}


function sendFiles(json_file) {
    const promise = new Promise((resolve, reject) => {
        console.log('======= SEND FILE ==========');
        sendBackgroundFiles(json_file.url, json_file.method, json_file.content, json_file.path, json_file.multipart, json_file.name_in_url)
            .then((r) => {
                if (r == 200 || r == 201 || r == 202) {
                    console.log('SEND FILE OK statusCode = ' + r);
                    ViewModel.set('response', 'Status code: ' + r);
                    files.removeFile(json_file.path)
                    resolve(r);
                } else {
                    // eslint-disable-next-line no-undef
                    alert('Error al enviar archivo, status code: ' + r, 3000);
                    reject(r);
                }
            })
            .catch((e) => {
                reject(e);
            });
    });
    return promise;
}


// BACKGROUND
const bghttp = require('@nativescript/background-http');
const session = bghttp.session('image-upload');
const files = require('~/common/files');

function sendBackgroundFiles(url, method, content, path, isMultipart, name_in_url) {
    if (isMultipart) {
        return multipartUpload(url, method, content, path, name_in_url);
    } else {
        return singleUpload(url, method, content, path, name_in_url);
    }
}

function getColor(name, code) {
    let color;
    switch (name) {
        case 'progress':
            color = 'white';
            break;
        case 'responded':
            color = 'silver';
            break;
        case 'complete':
            if (code == 200 || code == 201) {
                color = 'green';
            } else {
                color = 'red';
            }
            break;
        case 'cancelled':
            color = 'red';
            break;
        default: //error
            color = 'red';
            break;
    }
    return (color);
}

function onEvent(e) {
    const listView = ViewModel.get('listView');
    listView.unshift({
        color: getColor(e.eventName, e.responseCode),
        eventTitle: e.eventName + ' ' + e.object.description,
        eventData: JSON.stringify({
            error: e.error ? e.error.toString() : e.error,
            currentBytes: e.currentBytes,
            totalBytes: e.totalBytes,
            body: e.data,
            responseCode: e.responseCode
        })
    });
    ViewModel.set('listView', listView);
    page.getViewById('listView').refresh();

    switch (e.eventName) {
        case 'complete':
            console.log('========= BG complete ========= ', e.responseCode);
            setNextLoop(e.object.description, e.responseCode);
            break;
        case 'error':
            console.log('========= BG error ============', e.responseCode);
            setNextLoop(e.object.description, e.responseCode);
            break;
        default:
            break;
    }
}

function setNextLoop(file_name, status) {
    ls.setNumber('bg_loop_status', status);
    deleteLsJsonContent(file_name);
}

function deleteLsJsonContent(file_name) {
    ls.remove(file_name);
}

function getNameFromFile(item) {
    const last = item.lastIndexOf('/');
    return item.substring(last + 1, item.length);
}

function setLsJsonContent(file_name, content, path) {
    ls.setJson(file_name, {
        path: path,
        content: content
    });
}

function detectFinishBackground() {
    const promise = new Promise((resolve, reject) => {
        const interval = setInterval(() => {
            const status = ls.getNumber('bg_loop_status');
            console.log('status interval:', status);
            switch (status) {
                case 0:
                    //next loop
                    //reject(status);
                    break;
                case 200:
                    clearInterval(interval);
                    resolve(status);
                    break;
                case 201:
                    clearInterval(interval);
                    resolve(status);
                    break;
                default:
                    clearInterval(interval);
                    reject(status);
                    break;
            }
        }, 1000);
    });
    return promise;
}

function getParamsMultipart(items) {
    const content = [];
    for (const k in items) {
        content.push({
            name: k,
            value: items[k]
        })
    }
    return content;
}

function getExtension(name) {
    const last = name.lastIndexOf('.');
    return name.substring(last + 1, name.length);
}

function singleUpload(url, method, content, path, name_in_url) {
    console.log('singleUpload(()');
    const promise = new Promise((resolve, reject) => {

        const size = files.getSizeFile(path);
        const file_name = getNameFromFile(path);

        if (size > 0) {
            setLsJsonContent(file_name, content, path);
            console.log('size: ' + file_name + ' = ' + bytesToSize(size));
            console.log(method, url);
            console.log(path);
            console.log(content);
            console.log('name_in_url', name_in_url);
            const request = {
                url: url,
                method: method,
                headers: {
                    'x-device-type': 1,
                    'Authorization': 'Bearer ' + ViewModel.set('token'),
                    'Content-Type': ViewModel.get('contentType'), // 'multipart/form-data' 'form-data' 'application/octet-stream'
                    'file-name': name_in_url ? name_in_url : file_name,
                    'Content': JSON.stringify(content)
                },
                description: name_in_url ? name_in_url : file_name,
                AndroidAutoClearNotificación: true
            };

            for (const key in request) {
                if (key == 'headers') {
                    const request2 = request[key];
                    for (const key2 in request2) {
                        pushMessage('yellow', key + '->' + key2, request2[key2]);
                    }
                } else {
                    pushMessage('yellow', key, request[key]);
                }
            }

            const task = session.uploadFile(path, request);

            pushMessage('primary', 'content', JSON.stringify(content));

            ls.setNumber('bg_loop_status', 0);
            task.on('progress', onEvent);
            task.on('error', onEvent);
            task.on('responded', onEvent);
            task.on('complete', onEvent);
            task.on('cancelled', onEvent); // Android only
        } else {
            console.log('====== DELETE SIZE 0 ========', path);
            reject('sise 0');
        }

        detectFinishBackground()
            .then((r) => {
                console.log('statusA:', r);
                resolve(r);
            })
            .catch((e) => {
                console.log('errorA:', e);
                reject(e);
            });
    });
    return promise;
}

function bytesToSize(bytes) {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if (i == 0) return bytes + ' ' + sizes[i];
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
}

function multipartUpload(url, method, content, path, name_in_url) {
    console.log('multipartUpload()');
    const promise = new Promise((resolve, reject) => {

        const size = files.getSizeFile(path);
        const file_name = getNameFromFile(path);

        if (size > 0) {
            setLsJsonContent(file_name, content, path);
            console.log('size: ' + file_name + ' = ' + bytesToSize(size));
            const request = {
                url: url,
                method: method,
                headers: {
                    'x-device-type': 1,
                    'Authorization': 'Bearer ' + ViewModel.get('token'),
                    'Content-Type': ViewModel.get('contentType'), // 'multipart/form-data' 'form-data' 'application/octet-stream'
                    'file-name': name_in_url ? name_in_url : file_name
                },
                utf8: true,
                description: name_in_url ? name_in_url : file_name,
                AndroidAutoClearNotificación: true
            };
            for (const key in request) {
                if (key == 'headers') {
                    const request2 = request[key];
                    for (const key2 in request2) {
                        pushMessage('yellow', key + '->' + key2, request2[key2]);
                    }
                } else {
                    pushMessage('yellow', key, request[key]);
                }
            }

            console.log(request);
            console.log(typeof(content), content);

            let mimeType = ['image/jpg', 'video/mp4'];
            let extension = getExtension(file_name);
            extension = extension.toLowerCase();
            if (extension == 'jpg' || extension == 'jpeg') {
                mimeType = mimeType[0];
            } else if (extension == 'mp4') {
                mimeType = mimeType[1];
            } else {
                mimeType = extension
            }
            console.log('extension: ' + extension, 'mimeType: ' + mimeType);

            const params = getParamsMultipart(content); // ojo content is object
            params.push({ name: 'file', filename: path, mimeType: mimeType });
            const task = session.multipartUpload(params, request);

            pushMessage('yellow', 'mimeType', mimeType);
            pushMessage('primary', 'content', JSON.stringify(content));

            ls.setNumber('bg_loop_status', 0);
            task.on('progress', onEvent);
            task.on('error', onEvent);
            task.on('responded', onEvent);
            task.on('complete', onEvent);
            task.on('cancelled', onEvent); // Android only 
        } else {
            console.log('====== DELETE SIZE 0 ========', path);
            reject('sise 0');
        }

        detectFinishBackground()
            .then((r) => {
                console.log('statusA:', r);
                resolve(r);
            })
            .catch((e) => {
                console.log('errorA:', e);
                reject(e);
            });
    });
    return promise;
}

exports.tapShared = function() {
    console.log('tapShared()');
    let items = ViewModel.get('listView');
    if (items) {
        items = JSON.stringify(items);
        SocialShare.shareText(items);
    }
}