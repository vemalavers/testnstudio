const ViewModel = require('./camera-view-model');

const ls = require('~/common/ls');
const image = require('~/common/image');

let page;
exports.loaded = function(args) {
    page = args.object;
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
    page.bindingContext = ViewModel;
}

exports.tapAdd = function() {
    console.log('tapAdd()');
    const image_name = Date.now();
    image.takePicture(ls.getString('path_tmp'), ls.getString('platform'), image_name).
    then((r) => {
        console.log(r);
        if (r) {
            const item = [{
                name: image_name,
                src: r
            }]
            setContent(item);
        }
    }, (e) => {
        console.log(e);
    });
}

function setContent(item) {
    console.log('setContent()');
    page.getViewById('name').text = item[0].name;
    page.getViewById('path').text = item[0].src;
    page.getViewById('img').src = item[0].src;
}