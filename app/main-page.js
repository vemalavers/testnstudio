const ViewModel = require('./main-view-model');

const ls = require('~/common/ls');
const nav = require('~/common/nav');
const files = require('~/common/files');
const session = require('~/common/session');
const platform = require('~/common/platform');

let page;
exports.onNavigatingTo = function(args) {
    page = args.object;
    page.bindingContext = ViewModel;
    setInit();
}

function setInit() {
    setDimesions();
    setSession();
    setFolders();
}

function setSession(isDev) {
    //session.deleteConfig();
    ls.setBool('isDev', isDev ? true : false);
    const so = platform.getPlatform();
    const sdk = (so == 'android' ? platform.getSdkVersion() : 0);
    console.log(so, sdk);
    session.setConfig(so, sdk, isDev);
}

function setDimesions() {
    console.log('setDimesions()');
    let width = platform.getScreenDimensions('width');
    let height = platform.getScreenDimensions('height');
    if (typeof(width) == 'number' && typeof(height) == 'number') {
        width = parseInt(width);
        height = parseInt(height);
    } else {
        width = 411;
        height = 845;
    }
    if (height > width) {
        ls.setNumber('screenWidth', width);
        ls.setNumber('screenHeight', height);
        ViewModel.set('width', width);
        ViewModel.set('height', height);
    } else {
        ls.setNumber('screenWidth', height);
        ls.setNumber('screenHeight', width);
        ViewModel.set('width', height);
        ViewModel.set('height', width);
    }
    console.log('width', width);
    console.log('height', height);
}

function setFolders() {
    const path = files.getPath(ls.getString('platform'), true, false);
    ls.setString('path', path);

    const path_queue = files.newFolder(path, 'queue');
    const path_tmp = files.newFolder(path, 'tmp');
    const path_backup = files.newFolder(path, 'backup');

    ls.setString('path_queue', path_queue);
    ls.setString('path_tmp', path_tmp);
    ls.setString('path_backup', path_backup);
}

exports.tapComponents = function(args) {
    const item = args.object;
    nav.goTo(page.page, item.path, {}, 'slide', false, false);
}