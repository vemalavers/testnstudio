const ViewModel = require('./google-maps-view-model');

const gps = require('~/common/gps');
//const { MapView } = require('@nativescript/google-maps');

//const style = require('~/map-style.json');

let page;
exports.loaded = function(args) {
    page = args.object;
    page.bindingContext = ViewModel;
    ViewModel.set('latitude', 0);
    ViewModel.set('longitude', 0);
    ViewModel.set('loaded', true);
}
let map;
exports.onMapReady = function(args) {
    console.log('onMapReady()')
    map = args.object;
    //map.buildingsEnabled = true;
    //map.uiSettings.compassEnabled = true;
    //map.uiSettings.zoomControlsEnabled = true;

    //map.mapStyle(style);
    //map.myLocationButtonEnabled = true;
}
exports.tapGps = function() {
    tapGps();
}

function tapGps() {
    console.log('tapGps()');
    gps.getGeoLocation().
    then((r) => {
        //console.log(r);
        setContent(r);
    }, (e) => {
        console.log(e);
        // eslint-disable-next-line no-undef
        alert(e);
    });
}

function setContent(items) {
    console.log('setContent()');

    ViewModel.set('latitude', ViewModel.observer({
        latitude: items.latitude
    }));
    ViewModel.set('longitude', ViewModel.observer({
        latitude: items.longitude
    }));

    map.lat = items.latitude;
    map.lng = items.longitude;

    map.zoom = 20;
}

exports.tapMap = function() {
    console.log('tapMap()');
    console.log(map.lat);
    console.log(map.cameraPosition);
    console.log('---------');
}