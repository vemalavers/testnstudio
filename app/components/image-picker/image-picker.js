const ViewModel = require('./image-picker-view-model');

const ls = require('~/common/ls');
const dt = require('~/common/dt');
// const imagepicker = require('@nativescript/imagepicker');
// const { ImagePickerMediaType } = require('@nativescript/imagepicker');

let page;
exports.loaded = function(args) {
    page = args.object;
    ViewModel.set('width', ls.getNumber('screenWidth'));
    ViewModel.set('height', ls.getNumber('screenHeight'));
    page.bindingContext = ViewModel;
}

//MODAL CAMERA LOGIC
exports.tapAdd = function(args) {
    const list = [];
    const type = args.object.type;
    let mediaType;
    if (type == 'video') {
        mediaType = ImagePickerMediaType.Video;
    } else {
        mediaType = ImagePickerMediaType.Image;
    }
    const context = imagepicker.create({ mode: 'multiple', mediaType: mediaType }); // use 'multiple' or 'single' for multiple selection // Image/Video/Any
    context
        .authorize()
        .then(function() {
            return context.present();
        })
        .then(function(selection) {
            console.log(selection);
            selection.forEach(function(fileAsset) {
                list.push({
                    src: fileAsset._android,
                    captured_at: dt.formatDateTimeMySql(new Date()),
                    duration: 0,
                    rotate: 0
                })
            });
            setContent(list);
        }).catch(function() {});
}

function setContent(item) {
    console.log('setContent()');
    console.log(item);
    page.getViewById('name').text = item[0].name;
    page.getViewById('type').text = item[0].type;
    page.getViewById('captured_at').text = item[0].captured_at;
    page.getViewById('path').text = item[0].src;
    if (item[0].type == 'video') {
        console.log(page.getViewById('video'));
        page.getViewById('video').src = item[0].src;
        page.getViewById('image').visibility = 'collapsed';
        page.getViewById('video').visibility = 'visible';
    } else {
        page.getViewById('image').src = item[0].src;
        page.getViewById('video').visibility = 'collapsed';
        page.getViewById('image').visibility = 'visible';
    }
}