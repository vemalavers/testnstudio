const camera = require("@nativescript/camera");
const imageSource = require('@nativescript/core/image-source');

camera.requestPermissions();

exports.takePicture = function(path, platform, name) {
    const promise = new Promise((resolve, reject) => {
        takePicture(path, platform, name)
            .then((r) => {
                if (r) {
                    resolve(r);
                } else {
                    reject(r);
                }
            }).catch((e) => {
                reject(e);
            });
    })
    return promise;
}

function takePicture(path, platform, name) {
    console.log('path', path);
    console.log('platform', platform);
    console.log('name', name);

    const options = { width: 800, height: 600, keepAspectRatio: true, saveToGallery: false };
    const promise = camera.takePicture()
        .then(function(imageAsset) {
            imageAsset.options = options;
            return saveResizeImage(imageAsset, path, platform, name);
        })
        .then(function(r) {
            return r;
        }).catch(function(e) {
            console.log(e);
            return false;
        });
    return promise;
}

function saveResizeImage(imageAsset, path, platform, name) {
    const promise = new Promise((resolve, reject) => {
        const new_image_url = path + '/' + name + '.jpg';
        imageSource.ImageSource.fromAsset(imageAsset)
            .then(function(r) {
                const saved = r.saveToFile(new_image_url, 'jpg');
                if (saved) {
                    console.log(`Size end wxh: ${r.width}x${r.height}`);
                    resolve(new_image_url);
                } else {
                    reject('Error in save image ' + platform);
                }
                return r;
            }).catch(function(e) {
                console.log(e);
                return false;
            });
    });
    return promise;
}