const Http = require('@nativescript/core/http');
const ViewModel = require('./cards-view-model');

const ls = require('~/common/ls');
const dt = require('~/common/dt');
const nav = require('~/common/nav');
const encode = require('~/common/encode');

const message = require('~/custom/message/message');

let page;
exports.loaded = function(args) {
    page = args.object;
    const item = page.navigationContext;
    setInit(item);
    page.bindingContext = ViewModel;
}

function setInit(item) {
    ViewModel.set('loaded', true);
    ViewModel.set('title', 'CARDS + HTTP');
    ViewModel.set('offset', undefined);
    ViewModel.set('last_page', undefined);
    ViewModel.set('height', (ls.getNumber('screenWidth') - 58) * 3 / 4);
    for (const i in item) {
        ViewModel.set(i, item[i]);
    }
    getData();
}
exports.onLoadMoreItems = function() {
    if (encode.getOffset(ViewModel.get('offset'), ViewModel.get('last_page')) && ViewModel.get('loaded')) {
        getData();
    }
}
exports.tapBack = function() {
    nav.goBack(page.page);
}
exports.itemTap = function(args) {
    const listView = ViewModel.get('listView');
    const item = listView[args.index];
    nav.goTo(page.page, 'cards/details/details', item, 'slide', false, false);
}

function setStruct(items) {
    const aws = ls.getString('aws');
    const layout = getLayout();
    const height = ViewModel.get('height');
    items.forEach(item => {
        item.key = layout;
        item.height = height;
        item.title = item.name;
        item.subtitle = item.face_type;
        item.description = item.address;
        item.badge = item.element_type;
        item.footer = item.unit_type.name;
        item.subfooter = dt.getDatedif(item.updated_at);
        item.created_at = dt.formatDateTimeMySql(item.updated_at);
        item.image_url = aws + ls.imgSize(item.image_url, '_' + layout);
        item.icon = undefined;
    });
    return items;
}

function setData(items) {
    ViewModel.set('offset', items.current_page);
    ViewModel.set('last_page', items.last_page);
    ViewModel.set('sbHint', 'Total: ' + items.total);
    items = items.data;
    //ViewModel.set('sbHint', 'Total: ' + items.length);
    if (ViewModel.get('offset') == 1) {
        ViewModel.set('listView', setStruct(items));
    } else {
        ViewModel.set('listView', ViewModel.get('listView').concat(setStruct(items)));
    }
}

function getParams() {
    const sbText = (ViewModel.get('sbText') ? ViewModel.get('sbText') : '');
    const items = {
        offset: encode.getOffset(ViewModel.get('offset'), ViewModel.get('last_page')),
        limit: 20, //ls.getNumber('pag'),
        // fields: ['path', 'unit', 'type_face.name', 'type_unit', 'instruction', 'captured_at', 'captured_by'],
        sort: { 'id': 'desc' },
        contains: {
            fields: ['licence.name'],
            text: sbText
        },
        currency: 'PEN',
        //paginate: false
    }
    return items;
}

function getData() {
    const params = encode.encode(getParams());
    const url = 'licences?' + params;
    ViewModel.set('loaded', false);
    Http.request(ls.request2(url, 'GET')).then(function(r) {
        const items = JSON.parse(r.content);
        if (r.statusCode == 200) {
            setData(items);
        } else {
            nav.error(page.page, r.statusCode);
            message.messageShow('bg_red', '\uf102', 'Error al leer comprobaciones', items.message, 3000);
        }
        ViewModel.set('loaded', true);
    }, function(e) {
        console.log(e);
        ViewModel.set('loaded', true);
        message.messageShow('bg_red', '\uf102', 'Error de Internet', 'Verifique su conexión a internet', 3000);
    });
}
//LOGIC LAYOUT
function getLayout() {
    const layout = ViewModel.get('layout');
    return (layout == undefined ? 's' : layout);
}
exports.setLayout = function() {
    setLayout();
}

function setLayout() {
    const layout = ViewModel.get('layout');
    switch (layout) {
        case 's':
            ViewModel.set('layout', 'm');
            break;
        case 'm':
            ViewModel.set('layout', 'l');
            break;
        case 'l':
            ViewModel.set('layout', 's');
            break;
        default: // undefined
            ViewModel.set('layout', 'm');
            break;
    }
}

//logic search
function hideKeyBoard(searchBar) {
    if (searchBar.ios) {
        searchBar.ios.endEditing(true);
    } else if (searchBar.android) {
        searchBar.android.clearFocus();
    }
}
let timer = 0;
exports.setSearchBar = function(args) {
    clearTimeout(timer);
    const searchBar = args.object;
    hideKeyBoard(searchBar);
    searchBar.on('textChange', function() {
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function() {
            ViewModel.set('offset', undefined);
            ViewModel.set('last_page', undefined);
            if (ViewModel.get('loaded')) {
                getData();
            }
            timer = 0;
        }, 1000);
    });
}